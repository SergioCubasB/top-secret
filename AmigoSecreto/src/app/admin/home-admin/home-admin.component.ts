import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.scss']
})
export class HomeAdminComponent implements OnInit {
  state: string = "";
 
  name:String ="";

  status: boolean = true;
  statusPlayRulette: boolean;

  constructor(
    private router: Router,

    private sharedService: SharedService,
    private authService:  AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getName();
    this.getStatusActivation();

    this.sharedService.sendStateObservable.subscribe( response => {
      this.state = response;
    })
    
    //this.validateUser();

    this.route.queryParamMap
      .subscribe(params => {
          this.state = 'si';
      });
  }

  //============ ACTIVAR Y DESACTIVAR JUEGO DE RULETA ===============
  getStatusActivation() {
    this.sharedService.getStatusActivation()
      .subscribe((data: any) => {
        this.statusPlayRulette = data.data.data.stage;
      });
  }

  updateActivacion() {
    if (this.statusPlayRulette) {
      this.sharedService.updateDeactivate()
        .subscribe( response => {
          console.log(response);
          this.statusPlayRulette = !this.statusPlayRulette;
        });
    } else {
      this.sharedService.updateActivate()
        .subscribe( response => {
          this.statusPlayRulette = !this.statusPlayRulette;
        });
    }
  }

  getName(){
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
        console.log(data.data.data);
          const id = data.data.data.id;

        this.authService.getInformatioUserId(id)
        .subscribe( (dataUser:any )=> 
          {
            const {name, last_name, play} = dataUser.data.data;
            this.name = name +" "+ last_name;
            this.status = false;
          }
        )
      }
    )
  }

  go(){
    
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
          const { complete, play } = data.data.data

          if(complete === 1){
            this.router.navigate(['/start']);
          }
          else{
            this.router.navigate(['/formulario']);
          }
      }
    )
  }

  /* validateUser(){
    this.authService.getInfoUser()
    .subscribe((data:any)=> {
        
    });
  } */

  clickEvent(){
    this.status = !this.status;
  }

  closeSession(){

    Swal.fire({
      html:
      `
      <div class="container-session">
          <h5 class="container-rules__title Gotham-notbold"> Cerrar sesión</h5>
          <p class="Gotham-notbold">¿Está seguro que deseas cerrar sesión?</p>
      </div>
      `,
      showConfirmButton: true,
      showCancelButton: true,
      allowOutsideClick: false,
      width: '600px',
      customClass: {
        container: 'session',
        cancelButton: 'btn-primary Gotham-notbold',
        confirmButton: 'btn-anybg Gotham-notbold'
      },
      buttonsStyling: false,
      confirmButtonText: 'Si, Cerrar Sesión',
      cancelButtonText: 'No, mantenerme aquí',

    }).then((result) => {
      if (result.isConfirmed) {
        sessionStorage.clear();
        this.router.navigateByUrl('admin');
      }

    });
    
  }

}
