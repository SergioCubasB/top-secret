import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class SharedService{
    state: string = '';
    private sendStateSubject = new Subject<string>();
    sendStateObservable = this.sendStateSubject.asObservable();



    protected env = `${environment.api}/v1`;

    constructor(
        private http: HttpClient
    ){}

    validateOption(){

    }


    postDecisionAccept(){
        const token = sessionStorage.getItem("_TOKEN");
        const headers = new HttpHeaders({
        'Authorization': `Bearer ${token}`
        })

        return this.http.post(`${this.env}/user-auth/play-acept`, '', { headers });
    }

    postDecisionRefused(){
        const token = sessionStorage.getItem("_TOKEN");
        
        const headers = new HttpHeaders({
        'Authorization': `Bearer ${token}`
        })

        return this.http.post(`${this.env}/user-auth/play-refuse`, '', { headers });
    }

    getInfo(id: String){
        const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.get(this.env + `/user-auth/${id}`, { headers });
    }


    getQuestions(){
        const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.get(this.env + '/question?expand=alt', { headers });
    }


    postQuestionsUser(data){
        const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.post(this.env + '/user-auth/answer', data, { headers });
    }


    getMyFriend(formData: FormData){
        const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.post(this.env + '/user-auth/friend-detail', formData, { headers });
    }


    postSendEmail(data){
        const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.post(this.env + '/user-auth/send-email', data, { headers });
    }

    sendState(state: string){
      this.state = state;
      this.sendStateSubject.next(state);
    }



    getPdfUser(id){
      const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.get(this.env + `/user-auth/${id}/generate-pdf`, { headers:headers, responseType:"blob"});
    }

    getNameUsers() {
      const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.get(this.env + '/user-auth?fields=name', { headers });
    }

    getStatusActivation() {
      const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.get(this.env + '/process', { headers });
    }

    updateActivate() {
      const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.post(this.env + '/process/activate', null, { headers });
    }

    updateDeactivate() {
      const token = sessionStorage.getItem("_TOKEN");
    
        const headers = new HttpHeaders({
          'Authorization': `Bearer ${token}`
        })
    
        return this.http.post(this.env + '/process/deactivate', null, { headers });
    }

}