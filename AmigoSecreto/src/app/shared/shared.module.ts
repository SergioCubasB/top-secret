import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HeaderComponent } from "./component/header/header.component";
import { MaterialModule } from "./material/material.module";
import { PreloaderComponent } from './component/preloader/preloader.component';


@NgModule({
    declarations: [
        HeaderComponent,
        PreloaderComponent
    ],
    imports: [
        BrowserModule,
        MaterialModule,
    ],
    exports:[
        HeaderComponent,
        MaterialModule,
        PreloaderComponent
    ]
})
export class SharedModule{}