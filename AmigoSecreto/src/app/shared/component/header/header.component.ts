import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import Swal from 'sweetalert2';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  validate: boolean;

  showButtom: boolean;
  status: boolean = true;

  idUser: any;

  check = new FormGroup({
  valorCheck: new FormControl()
  });


  state: string = "";

  constructor(
    private router: Router,

    private sharedService: SharedService,
    private authService:  AuthService
  ) { }

  ngOnInit(): void {
    this.validateUser();

    this.check.reset();
  }

  clickEvent(){
    this.status = !this.status;
  }

  openDialog(){
    const token = sessionStorage.getItem("_TOKEN");

    if(this.showButtom || this.state === 'not'){
      Swal.fire({
        html:
        `
        <div class="container-rules">
  
            <h5 class="container-rules__title Gotham-notbold"> Reglas claras <span>¡Amistades duraderas! </span></h5>
            
            <ol class="container-rules__description Gotham-ligth-notbold">
                <li>Tu regalo deberá ser de S/ 50 como mínimo, ojo.</li>
                <li>Que no se te escape el nombre de tu amigo Cubo Rojo! Es <span class="Gotham">TOP SECRET</span></li>
                <li>Recuerda que una persona cuenta con tu compromiso en caso decidas participar del intercambio. ¡No hay vuelta atrás!</li>
            </ol>
        </div>
        `,
        showConfirmButton: true,
        showCancelButton: true,
        allowOutsideClick: false,
        width: '565px',
        customClass: {
          container: 'rules',
          cancelButton: 'btn-primary Gotham-notbold',
          confirmButton: 'btn-primary Gotham-notbold'
        },
        buttonsStyling: false,
        confirmButtonText: 'Acepto',
        cancelButtonText: 'No Acepto',
      }).then((result) => {
        if (result.isConfirmed) {
  
          this.sharedService.sendState("yes");

          this.sharedService.postDecisionAccept()
            .subscribe((data: any)=>{
              //console.log(data);
              //this.router.navigateByUrl('home',{ queryParams: { 'play': 1 } });
              this.router.navigate(['/home'], { queryParams: { play: 1 }});
            });
        }else{
          this.refused();

          this.sharedService.sendState("not");

          this.sharedService.postDecisionRefused()
            .subscribe((data: any)=>{
              console.log(data);
            });
  
        }
  
      });
    }else{

      Swal.fire({
        html:
        `
        <div class="container-rules">
  
            <h5 class="container-rules__title Gotham-notbold"> Reglas claras <span>¡Amistades duraderas! </span></h5>
            
            <ol class="container-rules__description Gotham-ligth-notbold">
                <li>Tu regalo deberá ser de S/ 50 como mínimo, ojo.</li>
                <li>Que no se te escape el nombre de tu amigo Cubo Rojo! Es <span class="Gotham">TOP SECRET</span></li>
                <li>Recuerda que una persona cuenta con tu compromiso en caso decidas participar del intercambio. ¡No hay vuelta atrás!</li>
            </ol>
        </div>
        `,
        showConfirmButton: false,
        showCancelButton: true,
        allowOutsideClick: true,
        width: '565px',
        customClass: {
          container: 'rules_accept',
          cancelButton: 'btn-primary Gotham-notbold',
        },
        buttonsStyling: false,
        cancelButtonText: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>'
      });
    }
    
    
  }

  closeSession(){

    Swal.fire({
      html:
      `
      <div class="container-session">
          <h5 class="container-rules__title Gotham-notbold"> Cerrar sesión</h5>
          <p class="Gotham-notbold">¿Está seguro que deseas cerrar sesión?</p>
      </div>
      `,
      showConfirmButton: true,
      showCancelButton: true,
      allowOutsideClick: false,
      width: '600px',
      customClass: {
        container: 'session',
        cancelButton: 'btn-primary Gotham-notbold',
        confirmButton: 'btn-anybg Gotham-notbold'
      },
      buttonsStyling: false,
      confirmButtonText: 'Si, Cerrar Sesión',
      cancelButtonText: 'No, mantenerme aquí',

    }).then((result) => {
      if (result.isConfirmed) {
        sessionStorage.clear();
        this.router.navigateByUrl('login');
      }

    });
    
  }


  refused(){
    Swal.fire({
      html:
      `
      <div class="container-session">
          <h5 class="container-rules__title Gotham-notbold"> Nos apena que no participes,</h5>
          <p class="Gotham-notbold">será para el próximo año</p>
      </div>
      `,
      showConfirmButton: false,
      showCancelButton: true,
      allowOutsideClick: false,
      width: '600px',
      customClass: {
        container: 'session',
        cancelButton: 'btn-primary Gotham-notbold',
        confirmButton: 'btn-anybg Gotham-notbold'
      },
      buttonsStyling: false,
      confirmButtonText: 'Si, Cerrar Sesión',
      cancelButtonText: 'Aceptar',

    }).then((result) => {
      if (!result.isConfirmed) {
        //sessionStorage.clear();
        //this.router.navigateByUrl('login');
      }

    });
  }

  validateUser(){
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
        console.log("DAta: ",data.data.data);
        
          const play = data.data.data.play;

          if(play === null || play === 0){
            this.showButtom = true;
            this.openDialog();
          }

          if(play === 1){
            this.showButtom = false;
          }
          
      }
    )
  }


}
