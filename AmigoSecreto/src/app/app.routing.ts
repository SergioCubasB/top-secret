import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth/guard/auth.guard";
import { LoginComponent } from "./auth/login/login.component";
import { AdminComponent } from "./auth/admin/admin.component";
import { FormularioComponent } from "./modules/formulario/formulario.component";
import { GameComponent } from "./modules/game/game.component";
import { HomeComponent } from "./modules/home/home.component";
import { RouletteComponent } from "./modules/roulette/roulette.component";
import { HomeAdminComponent } from "./admin/home-admin/home-admin.component";

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'game',
        component: GameComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'start',
        component: RouletteComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'formulario',
        component: FormularioComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin',
        component: AdminComponent
    },
    {
        path: 'home-admin',
        component: HomeAdminComponent,
        canActivate: [AuthGuard]
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}