import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, ValidationErrors} from '@angular/forms';
import { Router } from '@angular/router';
import { DominioEmail } from 'src/app/auth/login/dominioemail.validator';
import { AuthService } from 'src/app/auth/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  status: boolean = true;

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email, DominioEmail.validarDominioEmail]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private serviceAuth: AuthService,
    private authService:  AuthService,
    private navigation: Router
  ) { }

  ngOnInit(): void {

    setTimeout(()=> {
        this.status = false;
    }, 3000);

  }

  get f(){
    return this.form.controls;
  }


  onSubmit() {
    // this.navigation.navigateByUrl('home');
    if (this.form.invalid) {
      return;
    }

    const formData = new FormData();

    formData.append('email', this.form.value.email);
    formData.append('password', this.form.value.password);

    this.serviceAuth.postAdmin(formData).subscribe(
      (res: any) => {
        if (res.data.status !== 200) {
          console.log('credenciales invalidas :>> ');
          Swal.fire({
            html:
            `
            <div class="container-register__exit">
                <h2 class="Gotham"> ¡${res.data.mensaje}! </h2>
                
                <p class="Gotham-ligth">Asegúrate de haber ingresado tu correo corporativo y contraseña correctamente.</p>
            </div>
            `,
            showConfirmButton: false,
            showCloseButton: false,
            showCancelButton: true,
            focusCancel: false,
            customClass: {
              container: 'form-register',
            },
            cancelButtonText:
              '<img src="../../../assets/icons/close.svg">',
            width: '540px',
            })

          return;
        }
        sessionStorage.setItem('_TOKEN', res.data.data.tk);
        console.log('res.data.data :>> ', res.data.data);
        this.router.navigateByUrl('home-admin');
      },
      (err) => {
        console.log('error al iniciar session :>> ', err);

        

      }
    );
  }

  
  submit(){
    console.log(this.form.value);

    


  }

}
