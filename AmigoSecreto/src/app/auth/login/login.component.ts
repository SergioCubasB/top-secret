import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, ValidationErrors} from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/services/shared.service';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';
import { DominioEmail } from './dominioemail.validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  status: boolean = true;

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email, DominioEmail.validarDominioEmail]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private serviceAuth: AuthService,
    private authService:  AuthService,
    private navigation: Router,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {

    setTimeout(()=> {
        this.status = false;
    }, 3000);

  }

  get f(){
    return this.form.controls;
  }


  onSubmit() {
    // this.navigation.navigateByUrl('home');
    if (this.form.invalid) {
      return;
    }

    const formData = new FormData();

    formData.append('email', this.form.value.email);
    formData.append('password', this.form.value.password);

    this.serviceAuth.postLogin(formData).subscribe(
      (res: any) => {
        if (res.data.status !== 200) {
          console.log('credenciales invalidas :>> ');
          Swal.fire({
            html:
            `
            <div class="container-register__exit">
                <h2 class="Gotham"> ¡Credenciales incorrectas! </h2>
                
                <p class="Gotham-ligth">Asegúrate de haber ingresado tu correo corporativo y contraseña correctamente.</p>
            </div>
            `,
            showConfirmButton: false,
            showCloseButton: false,
            showCancelButton: true,
            focusCancel: false,
            customClass: {
              container: 'form-register',
            },
            cancelButtonText:
              '<img src="../../../assets/icons/close.svg">',
            width: '540px',
            })

          return;
        }
        sessionStorage.setItem('_TOKEN', res.data.data.tk);
        console.log('res.data.data :>> ', res.data.data);

        //===== INICIAR CON JUEGO DE LA RULETA =====
        //const startPlay = true;
        
        //==========================================
        //this.router.navigateByUrl('home');
        this.authService.getInfoUser()
          .subscribe((dataUser: any) => {
            this.sharedService.getStatusActivation()
              .subscribe((dataStatus: any) => {
                if (dataUser.data.data.complete == 1) {
                  if (dataStatus.data.data.stage) {
                    this.router.navigateByUrl('game');
                  } else {
                    this.router.navigateByUrl('start');
                  }
                } else {
                  this.router.navigateByUrl('home');
                }
              });
          });
      },
      (err) => {
        console.log('error al iniciar session :>> ', err);
      }
    );
  }

  
  submit(){
    console.log(this.form.value);

    


  }

}
