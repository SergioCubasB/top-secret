import { FormControl, ValidationErrors } from "@angular/forms";

export class DominioEmail {
  static validarDominioEmail(control: FormControl): ValidationErrors {
    let dominio = 'cuborojo.pe'
    if (control.value.includes(dominio))
      return null;
    else
      return { dominioemail: true }
  }
}
