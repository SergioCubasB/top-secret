import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  protected env = `${environment.api}/v1`;

  constructor(
    private http: HttpClient
  ) { }

  postLogin(formData: FormData) {
    return this.http.post(`${this.env}/user/signin`, formData)
  }

  postAdmin(formData: FormData) {
    return this.http.post(`${this.env}/user/signin-admin`, formData)
  }

  getInfoUser() {
    const token = sessionStorage.getItem("_TOKEN");
    
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(`${this.env}/user-auth/info`, {headers})
  }

  getInformatioUserId(id) {

    const token = sessionStorage.getItem("_TOKEN");
    
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(`${this.env}/user-auth/${id}`, {headers})
  }
  
}
