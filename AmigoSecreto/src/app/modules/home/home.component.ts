import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { SharedService } from 'src/app/shared/services/shared.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  state: string = "";
 
  name:String ="";
  showButtom: boolean;

  showButtomNextForm: boolean = true;
  status: boolean = true;

  play: boolean = false;

  constructor(
    private router: Router,

    private sharedService: SharedService,
    private authService:  AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getName();

    this.sharedService.sendStateObservable.subscribe( response => {
      this.state = response;
    })
    
    this.validateUser();

    this.route.queryParamMap
      .subscribe(params => {
          this.play = (params.get('play') ? true : false) || false;
          this.showButtomNextForm = true;
          this.state = 'si';
      });
  }

  getName(){
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
        console.log(data.data.data);
          const id = data.data.data.id;

        this.authService.getInformatioUserId(id)
        .subscribe( (dataUser:any )=> 
          {
            const {name, last_name, play} = dataUser.data.data;
            this.name = name +" "+ last_name;
            this.status = false;
          }
        )
      }
    )
  }

  go(){
    
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
          const { complete, play } = data.data.data

          if(complete === 1){
            this.router.navigate(['/start']);
          }
          else{
            this.router.navigate(['/formulario']);
          }
      }
    )
  }

  validateUser(){
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
          this.play = data.data.data.play;
          this.showButtomNextForm = false;
          if(this.play && data.data.data.complete == 0){
            this.showButtomNextForm = true;
          }

      }
    )
  }



  

}
