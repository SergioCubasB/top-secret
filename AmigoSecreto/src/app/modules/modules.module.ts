import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { ModulesComponent } from "./modules.component";
import { SharedModule } from "../shared/shared.module";
import { AppRoutingModule } from "../app.routing";
import { RouterModule } from "@angular/router";
import { OwlModule } from 'ngx-owl-carousel';
import { RouletteComponent } from './roulette/roulette.component';
import { FormularioComponent } from './formulario/formulario.component';

@NgModule({
    declarations: [
        ModulesComponent,
        RouletteComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        SharedModule,
        RouterModule,
        OwlModule
    ],
    exports: [
        OwlModule
    ]
})

export class ModulesModule {}