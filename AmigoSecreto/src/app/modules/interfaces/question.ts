export interface Question{
    id: Number,
    title: String,
    alternative: Array<alternative>,
    type: String
}

export interface alternative{
    text: String,
    value: String
}