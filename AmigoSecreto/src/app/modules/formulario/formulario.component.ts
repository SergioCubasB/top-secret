import { Component, Input, OnInit, ViewChild } from '@angular/core';


import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {MatRadioChange, MatRadioModule} from '@angular/material/radio';
import { Question } from '../interfaces/question';

import { SlidesOutputData, OwlOptions } from 'ngx-owl-carousel-o';
import { ViewportScroller } from '@angular/common';
import { MatInput } from '@angular/material/input';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/services/shared.service';
import { MatCheckbox, MatCheckboxChange, MatCheckboxClickAction } from '@angular/material/checkbox';
import { AuthService } from 'src/app/auth/services/auth.service';
import { OwlCarousel } from 'ngx-owl-carousel';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})

export class FormularioComponent implements OnInit {
  public  defaultSelected = 0
  public selection: number

  questions: [];

  active_form: string = 'false';
  finalArray: any[] = [];

  arrayChecked: any = [];
  conditionCheck: boolean = false;

  idUser: any;

  rpta: boolean = false;
  nro_seccion: number = 0;

  @ViewChild('owlElement') owlElement: OwlCarousel;

  status: boolean = true;
  formGroup: FormGroup;

  num=0;

  hideCheckBox: any;
  constructor(
    public viewportScroller: ViewportScroller,
    private router: Router,

    private shareService: SharedService,
    private authService:  AuthService,
    private formBuilder: FormBuilder
  ) {

    this.formGroup = formBuilder.group({
      rick: false,
      reggaeton: false,
      indie: false,
      metal: false,
      pop: false,
      salsa: false,
      latin: false,
      cumbia: false
    });

   }


  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['<img src="../../../assets/icons/arrow_carousel_left.png">', '<img src="../../../assets/icons/arrow_carousel.png">'],
    
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }

  ngOnInit(): void {
    this.authService.getInfoUser()
    .subscribe(( data:any )=> {
          const { id, complete } = data.data.data;
          this.idUser = id;
      });
    this.getQuestions();
  }

  btnPrev(owl) {
    this.rpta = true;
    owl.prev();
  }

  btnNext(ev) {
    this.questions.forEach((e,i) => {
      if (i == ev + 1) {
        if (this.rpta === true || this.nro_seccion > i) {
          this.owlElement.next();
          this.rpta = false;
        }
      }
    });
    if(this.arrayChecked.length >= 1 && this.num == 8){
      this.num = 0;
      if(!this.finalArray.some(x => x.id == 8)){
        if (this.arrayChecked.length == 2) {
          this.conditionCheck = true;
        }
        this.finalArray.push({
          id: 8,
          selected: this.arrayChecked
        });
      }

    }
  }


  radioChange(event: MatRadioChange, data) {
    
    var obj:any = this.questions.filter( (x:any) => x.id == data.id)[0];

    obj.selected = event.value;
    if (!this.finalArray.some(x => x.id == data.id)) {
      this.finalArray.push({
        id: data.id,
        selected: event.value
      });
    }else{
      this.finalArray.forEach( (indice, value) => {
        if(indice.id == data.id){
          this.finalArray[value].selected = event.value;
        }
      })
    }
    this.validateForm();

    this.rpta = true;
    if (data.id > this.nro_seccion) {
      this.nro_seccion = data.id;
    }
  }

  onChkChange(event: any, data, idCheck) {
    if (event.checked) {
      //if(this.arrayChecked.length < 0){
        this.arrayChecked.push({
          id: idCheck,
          selected: event.source.value
        });
      /* }else{
        
        if(!this.arrayChecked.some(x => x.id == event.source.value)){
          this.arrayChecked.push({
            id: idCheck,
            selected: event.source.value
          });

        }else{

          this.arrayChecked.push({
            id: idCheck,
            selected: event.source.value
          });

        }

      } */

      this.num = 8;

      if(this.arrayChecked.length === 2){
        setTimeout( ()=> {
          this.conditionCheck = true;
        },1000)
      }
      this.validateForm();

      this.rpta = true;
      if (data.id > this.nro_seccion) {
        this.nro_seccion = data.id;
      }
    }
  }

  textChange(event: any, data) {

    if (!this.finalArray.some(x => x.id == data)) {
      this.finalArray.push({
        id: data,
        selected: event.target.value
      });
    }else{
      this.finalArray.forEach( (indice, value) => {
        if(indice.id == data){
          this.finalArray[value].selected = event.target.value;
        }
      })

    }

    if (event.target.value != '') {
      this.rpta = true;
      if (data.id > this.nro_seccion) {
        this.nro_seccion = data.id;
      }
    } else {
      this.rpta = false;
    }

    this.validateForm();

  }

  
  changeBg($element): void {
    let background = document.getElementById("container-home");

    this.active_form = 'true';

    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    localStorage.setItem('form_questions', this.active_form);

    background.style.background  = "url('../../../assets/img/home/bg-2.png')";

  }

  
  validateForm(){ 
    var longitud = this.finalArray.length;
    if(this.questions.length == longitud){
      
      if (this.finalArray[this.finalArray.length -1].selected.length >= 4) {

        Swal.fire({
          html:
          `
          <div class="container-session">
              <h5 class="container-rules__title Gotham-notbold">¡Estamos a punto de guardar todas tus respuestas!</h5>
          </div>
          `,
          showConfirmButton: true,
          showCancelButton: true,
          allowOutsideClick: false,
          width: '600px',
          customClass: {
            container: 'session',
            cancelButton: 'btn-anybg Gotham-notbold',
            confirmButton: 'btn-primary Gotham-notbold'
          },
          buttonsStyling: false,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Aún no',
    
        }).then((result) => {
          if (result.isConfirmed) {
            this.registro(longitud);
          }
        });
      }
    }

  }

  registro(longitud: number) {
    const formData = new FormData();
    
    formData.append(`user_id`,this.idUser);      
    for (let index = 0; index < longitud; index++) {
      
      formData.append(`answer[${index}][question_id]`,this.finalArray[index].id);      

      if(typeof(this.finalArray[index].selected) === 'object'){
        this.finalArray[index].selected.forEach( (element:any, indexTwo: any)=>{
          formData.append(`answer[${index}][alternative][${indexTwo}]`,element.id);
        })
      }else{
        formData.append(`answer[${index}][alternative][0]`,this.finalArray[index].selected);
      }
    }

    this.shareService.postQuestionsUser(formData)
    .subscribe( ( data: any )=> {
      
    })
    

    Swal.fire({
      html:
      `
      <div class="container-register__exit">
          <h2 class="Gotham"> ¡Todas tus respuestas <br> se registraron con éxito! </h2>
          <img src="../../../assets/icons/success.svg" alt="">
          
          <p class="Gotham-ligth">Nos aseguraremos de que este año<br>
          te regalen algo que te guste (;</p>
      </div>
      `,
      showConfirmButton: false,
      showCloseButton: false,
      showCancelButton: true,
      focusCancel: false,
      customClass: {
        container: 'form-register',
      },
      cancelButtonText:
        '<img src="../../../assets/icons/close.svg">',
      width: '600px',
      }).then((result) => {
        if (!result.isConfirmed) {
          localStorage.setItem('user_register', "true");
          this.router.navigate(['/start']);
        }
      });
  }


  /* validateUser(){
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
          const { id } = data.data.data
          this.idUser = id;
      }
    )
  } */

  getQuestions(){
    this.shareService.getQuestions()
    .subscribe( (data: any) => {
      this.status = false;

      this.questions = data.data.data;
    })

  }

}
