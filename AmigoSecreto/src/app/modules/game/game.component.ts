import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  nombres: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.cargarDatos();
  }

  cargarDatos() {
    this.authService.getInfoUser()
    .subscribe(
      ( data:any )=> {
        console.log(data.data.data);
          this.nombres = data.data.data.names;
      });
  }

  showGame(){
    console.log("dex");
    
  }

}
