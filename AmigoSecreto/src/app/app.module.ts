import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app.routing';
import { SharedModule } from './shared/shared.module';
import { ModulesModule } from './modules/modules.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';

import { CarouselModule } from 'ngx-owl-carousel-o';

import { HomeComponent } from './modules/home/home.component';
import { GameComponent } from './modules/game/game.component';
import { FormularioComponent } from './modules/formulario/formulario.component';
import { HomeAdminComponent } from './admin/home-admin/home-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GameComponent,
    FormularioComponent,
    HomeAdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    ModulesModule,
    SharedModule,
    BrowserAnimationsModule,
    CarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
